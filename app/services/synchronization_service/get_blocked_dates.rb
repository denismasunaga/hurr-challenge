module SynchronizationService
  class GetBlockedDates
    require 'net/http'
    require 'uri'
    require 'json'

    def self.run(user_email:, user_id:)
      new(user_email: user_email, user_id: user_id).run
    end

    def run
      get_blocked_dates
    end

    private

    def initialize(user_email:, user_id:)
      @user_email = user_email
      @user_id = user_id
    end

    def get_blocked_dates
      return [] if @user_email.blank?

      uri = URI.parse("#{ENV['EXTERNAL_API_URL']}/get-user-blocked-dates")

      response = HTTParty.post("#{ENV['EXTERNAL_API_URL']}/get-user-blocked-dates",
                               body: {email: @user_email},
                               headers: { 'Authorization' => ENV['EXTERNAL_API_SECRET_KEY'] }
                               )
      # header = { 'Authorization' => ENV['EXTERNAL_API_SECRET_KEY'] }
      # body = { 'email' => @user_email }
      #
      # http = Net::HTTP.new(uri.host, uri.port)
      # request = Net::HTTP::Post.new(uri.request_uri, header)
      # request.body = body.to_json
      #
      # response = http.request(request)
      result = JSON.parse response.body

      return [] if result.blank?

      StoreBlockedDateWorker.perform_async(result, @user_id)
    rescue StandardError => e
      nil
    end
  end
end