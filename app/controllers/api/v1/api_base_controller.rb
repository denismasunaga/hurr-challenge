module Api
  module V1
    class ApiBaseController < ApplicationController
      private

      def request_token
        request.headers['Authorization'].try(:gsub, /^Bearer /, '')
      end

      def validate_token
        valid = AuthTokenService::AuthTokenValidate.run(token: request_token)

        render json: { message: 'Invalid token' }, status: :unprocessable_entity unless valid
      end
    end
  end
end
