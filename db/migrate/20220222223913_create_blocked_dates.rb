class CreateBlockedDates < ActiveRecord::Migration[6.1]
  def change
    create_table :blocked_dates do |t|
      t.date :starting_date
      t.date :ending_date

      t.timestamps
    end
  end
end
