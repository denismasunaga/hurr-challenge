class User < ApplicationRecord
  devise :database_authenticatable, :registerable,
         :validatable

  after_create :sync_blocked_dates
  has_many :blocked_dates

  private

  def sync_blocked_dates
    SynchronizationService::GetBlockedDates.run(user_id: id, user_email: email)
  end
end
