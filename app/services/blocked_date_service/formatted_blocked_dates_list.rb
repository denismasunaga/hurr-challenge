module BlockedDateService
  class FormattedBlockedDatesList
    def self.run(blocked_dates:)
      new(blocked_dates: blocked_dates).run
    end

    def run
      format_dates
    end

    private

    def initialize(blocked_dates:)
      @blocked_dates = blocked_dates
    end

    def format_dates
      return [] if @blocked_dates.blank?

      formatted_dates = []
      @blocked_dates.each do |blocked_date|
        formatted_dates << { from: blocked_date.starting_date.strftime("%d-%m-%Y"), to: blocked_date.ending_date.strftime("%d-%m-%Y") }
      end

      formatted_dates
    end
  end
end