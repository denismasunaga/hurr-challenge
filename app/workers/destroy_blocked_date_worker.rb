class DestroyBlockedDateWorker
  include Sidekiq::Worker
  sidekiq_options retry: 5
  sidekiq_options queue: :alpha_default

  def perform(blocked_dates, user_id)
    SynchronizationService::DestroyBlockedDates.run(blocked_dates: blocked_dates, user_id: user_id)
  end
end
