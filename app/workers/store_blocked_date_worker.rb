class StoreBlockedDateWorker
  include Sidekiq::Worker
  sidekiq_options retry: 5
  sidekiq_options queue: :alpha_default

  def perform(blocked_date, user_id)
    SynchronizationService::ProcessBlockedDates.run(blocked_dates: blocked_date, user_id: user_id)
  end
end
