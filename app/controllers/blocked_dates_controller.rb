class BlockedDatesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_blocked_date, only: %i[ show edit update destroy ]

  # GET /blocked_dates or /blocked_dates.json
  def index
    @blocked_dates = current_user.blocked_dates
  end

  # GET /blocked_dates/1 or /blocked_dates/1.json
  def show
  end

  # GET /blocked_dates/new
  def new
    @blocked_date = BlockedDate.new
    @formatted_blocked_dates = BlockedDateService::FormattedBlockedDatesList.run(blocked_dates: current_user.blocked_dates)
  end

  # GET /blocked_dates/1/edit
  def edit
  end

  # POST /blocked_dates or /blocked_dates.json
  def create
    @blocked_date = BlockedDate.new(BlockedDateService::SplitDate.run(params: blocked_date_params))

    respond_to do |format|
      if @blocked_date.save
        send_blocked_date(true)
        format.html { redirect_to blocked_dates_path, notice: "Blocked date was successfully created." }
        format.json { render :show, status: :created, location: @blocked_date }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @blocked_date.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /blocked_dates/1 or /blocked_dates/1.json
  def update
    respond_to do |format|
      if @blocked_date.update(blocked_date_params)
        format.html { redirect_to blocked_date_url(@blocked_date), notice: "Blocked date was successfully updated." }
        format.json { render :show, status: :ok, location: @blocked_date }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @blocked_date.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /blocked_dates/1 or /blocked_dates/1.json
  def destroy
    @blocked_date.destroy
    send_blocked_date(false)

    respond_to do |format|
      format.html { redirect_to blocked_dates_url, notice: "Blocked date was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_blocked_date
    @blocked_date = BlockedDate.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def blocked_date_params
    permitted_params = params.fetch(:blocked_date, {}).permit(:date_range, :user_id)
  end

  def send_blocked_date(create)
    SendBlockedDateWorker.perform_async(
      [{
         id: @blocked_date.id,
         starting_date: @blocked_date.starting_date,
         ending_date: @blocked_date.ending_date
       }],
      current_user.email,
      create
    )
  end

end
