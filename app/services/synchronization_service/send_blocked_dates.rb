module SynchronizationService
  class SendBlockedDates
    require 'net/http'
    require 'uri'
    require 'json'

    def self.run(user_email:, blocked_dates:, create:)
      new(user_email: user_email, blocked_dates: blocked_dates, create: create).run
    end

    def run
      send_blocked_dates
    end

    private

    def initialize(user_email:, blocked_dates:, create:)
      @user_email = user_email
      @blocked_dates = blocked_dates
      @create = create
    end

    def send_blocked_dates
      return [] if @user_email.blank?

      endpoint = @create ? 'send-user-blocked-dates' : 'destroy-user-blocked-dates'
      response = HTTParty.post(
        "#{ENV['EXTERNAL_API_URL']}/#{endpoint}",
        body: {
          email: @user_email,
          blocked_dates: @blocked_dates
        },
        headers: { 'Authorization' => ENV['EXTERNAL_API_SECRET_KEY'] }
      )

      return true if response.code == 200

      false
    rescue StandardError
      nil
    end
  end
end