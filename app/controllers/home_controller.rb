class HomeController < ApplicationController
  before_action :authenticate_user!

  def index
    @date = BlockedDate.new
  end
end
